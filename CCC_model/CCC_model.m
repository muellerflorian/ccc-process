%% MATLAB SCRIPT TO calculate competition ChIP turnover curves 
%
% Model  is described in Lickwar et al, Nature. 2012 Apr 11;484(7393):251-5. 
%
%
% Script allows generating turnover curves for user-defined induction
% kinetics of the competitor protein. These turnover curves are generated
% for a range of turnover rates (dissociation rates). These curves can then
% be used to determine the detection limit of Competition ChIP. For a
% certain upper limit of these rates the will look identical to the
% competitor induction kinetics and can therefore not be distinguished
% anymore.
%
% Florian Mueller, muef@gmx.net
% v1, August 2012
%
% ==== NOTES
% Best used in CELL-MODE (see Matlab help file). This allows executing 
% one block with code after the other. 


%% Prepare workspace
clear all; close all; clc

%% == Read-in protein level from file
% Define in a tab-delimited text file. First row is a header row, each 
% subsequent row contains one time point. The first column stores time
% in seconds, the second column the relative level of competitor protein in
% in the total protein pool, i.e. the ratio of concentration of competitor 
% total protein concentration. It is not necessary to have the same 
% time-points as for teh sampling. 
%  + Example: Rap1_Protein_Final_110919.txt

[time_prot prot_rel_v status_protein] = file_load_protein_v1([]);
if status_protein
    parameters.protein.par(:,1) = time_prot;
    parameters.protein.par(:,2) = prot_rel_v;        
end



%% Define input parameters for model 

%== Define vector with different turnover rates that should be sampled 
%   Units: 1/s
tor_v = [1/10 1/100 1/500 1/1000 1/2500 1/5000 1/10000];

%== Define sampling time-points (in minutes)
time_min = [0; 10; 20; 30; 40; 50; 60; 90; 120; 150];


%% Calcuate turnover curvesTurn-over ratio


%== Assign model parameters
parameters.time         = time_min *60;     
parameters.protein.rel  = parameters.protein.par(:,2);
parameters.protein.time = parameters.protein.par(:,1);
parameters.protein.abs  = parameters.protein.rel ./ (1-parameters.protein.rel);


par_mod{1} = parameters.time;          % Time for turn-over ratio
par_mod{2} = parameters.protein.time;  % Time for protein
par_mod{3} = parameters.protein.rel;   % Relative protein level
par_mod{4} = 0;                        % IC = 0
par_mod{5} = 0.0;                      % BGd
flags.plot = 1;

%- Turn over ratio
for i_sim =1: length(tor_v)    
    
    tor = tor_v(i_sim);
    R_ccc = ccc_fun_v1(tor,par_mod,0);

    R_ccc_all(:,i_sim) = R_ccc;
    
    if flags.plot == 1;
        h_fig = figure;hold on
        plot(parameters.protein.time,parameters.protein.abs,'k','LineWidth',1.5)
        plot(parameters.time,R_ccc,'r','LineWidth',1.5)
        hold off
        box on
        xlabel('Time (s)'); ylabel('Protein level | Turnover ratio')
        legend('Protein level', ['Turn-over; rate = ', num2str(tor)],2)  
        set(h_fig,'Color','w')
    end
end


%% Plot all curves

%- Generate legend for display
N_legend = length(tor_v)+1;
M = cell(N_legend,1);
M(1,1) = {'Protein level'};
for i=2:N_legend
    M(i,1) = {['TO; r=', num2str(tor_v(i-1))]};   
end

%- Generate plot
h_fig = figure;hold on
plot(parameters.protein.time,parameters.protein.abs,'k','LineWidth',1.5)
plot(parameters.time,R_ccc_all,'LineWidth',1.5)
hold off
box on
xlabel('Time (s)'); ylabel('Protein level | Turnover ratio')
legend(M,2)  
set(h_fig,'Color','w')

