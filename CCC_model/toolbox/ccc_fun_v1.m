function R_ccc = ccc_fun_v1(parFit,par_mod,flagOutput)
  
R_ccc = ratio_fun(parFit,par_mod);

if flagOutput
    figure, hold on
    plot(par_mod{1},R_ccc,'-or')
    hold off
    box on
    xlabel('Time (min)')   
end

%-- Solve differential equation with Runge Kutta
function R_calc_bgd = ratio_fun(parFit,par_mod)
time      = par_mod{1};
time_prot = par_mod{2};
prot_rel  = par_mod{3};
IC        = par_mod{4};
bgd_p     = par_mod{5};

[T,P] = ode45(@(t, y) dy_fun(t,y,prot_rel,time_prot,parFit),time,IC);
R_calc     = (P./(1-P));

%- Consider background
p = (bgd_p/(1-bgd_p))/2;                 % Background as relative percentage (percent of IP)
R_calc_bgd = (R_calc + p*(R_calc+1))./(1 + p*(R_calc+1));


%-- Function for ode45 (Runge-Kutta)
% see Matlab help for more details
function dy = dy_fun(t,y,RT,time,parFit)



R = interp1(time,RT,t);         % Interpolate the data-set (RT,time) at time t
alpha = parFit(1);

dy = alpha*( R - y);





