function [time_prot prot_level_rel status_protein] = file_load_protein_v1(file_name_full)

% Load protein levels

if isempty(file_name_full)
    [fileName,pathName] = uigetfile('.txt','Load file with protein levels') ;
    file_name_full = fullfile(pathName, fileName);
else
    fileName = 1;
end
    
if fileName ~= 0

    fid = fopen(file_name_full);
    protein_struct = textscan(fid, '%f %f','HeaderLines',1,'delimiter','\t');
    fclose(fid);

    time_prot       = protein_struct{1};
    prot_level_rel  = protein_struct{2};
    status_protein  = 1;
else
    time_prot       = [];
    prot_level_rel  = [];
    status_protein  = 0;
end
