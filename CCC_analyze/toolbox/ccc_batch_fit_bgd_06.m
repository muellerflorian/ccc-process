function [fit_summary_all_min data_fit data_fit_all] =  ccc_batch_fit_bgd_06(parameters,options)
% SCRIPT TO FIT DATA FROM COMPETITION CHIP EXPERIMENTS
%
%
% INPUT
%    time_prot      ... Time-points in seconds; should be a column vector (size(time_prot,2)=1) 
%    prot_level_rel ... Relative protein levels; again a column vector
%
%    time_ccc       ...  Time-points in seconds, don't have to be the same ones as for the protein levels
%                        Can be a row vector (like in the examples in the Excel file)
%                     
%    R_ccc_lin      .... Turn-over ratio for different promoter - linear scale and not log scale!
                     %   - Each promoter is in one row
                     %   - Columns correspond to time-points
                     % See Excel file 'Colin_ccc_data.xls' and sheet
                     % 'Data_ccc' for an example of two different
                     % promoters. 
%
% OUTPUT
%  data_fit_all .... matrix: each row corresponds to one promoter. First
%                    the actual data-points are listed and then the actual fit.
%                    See sheet 'Data_ccc_fit'       
% fit_summary_all_min  ... In each row the best fit for one promoter is summarized. 
%                          1st col: starting values of turn-over ratio
%                                   which gave the best fit
%                          2nd col: Turn-over-rate of best fit
%                          3rd col: squared sum of residuals of the fit
%                          4th col: Genomic background of this promoter
%
% Florian Mueller, muef@gmx.net
%  - National Institutes of Health, LRBGE
%  - Institut Pasteur, Computational Imaging and Modeling Group


%% Some parameters for the processing
time_ccc       = parameters.time_ccc; 
R_ccc_lin      = parameters.R_ccc_lin; 
time_prot      = parameters.time_prot; 
prot_level_rel = parameters.prot_level_rel; 

flag_struct   = options.flag_struct; 
flagBGD       = flag_struct.flagBGD;                         

%% Fit all data
orient_data = 1;                % Orientation of Competition chip-data (1 in rows, 2 in columns)
%flagBGD     = 4;

par_mod{2} = time_prot;          % For protein production
par_mod{3} = prot_level_rel;
par_mod{4} = 0;                  % IC = 0

%- Different starting guesses (adjusted for Rap1 promoter)
part_start_RT = [100 250 500 1000 2000 50000 10000];
par_start_v   = 1./part_start_RT;

flagOutput  = 0;

if orient_data == 2
    nData = size(R_ccc_lin,2);
elseif orient_data ==1
    nData = size(R_ccc_lin,1);
end

 
%- Allocate memory
fit_summary_all(1,nData) = struct( 'values', [], 'curves_exp', [], 'curves_fit',[], 'ind_fit',[]);
 
%- Loop over data-set
if flag_struct.parallel 
    
    
    parfor iData = 1:nData 
        
        display(['Fit data-set ',num2str(iData)])
        
        %- Get data
        R_exp = [];
        if orient_data == 2
            R_exp = R_ccc_lin(:,iData);
        elseif orient_data ==1
            R_exp = R_ccc_lin(iData,:);
        end
                    
        %- Check is necessary to catch turn-over which couldn't be
        %  determined. All values are zero on log 2 scale - 1 in linear
        if not(sum(R_exp==1) == length(R_exp))
        
        
            %- Check for NAN and leave them out!
            ind_not_nan = ~isnan(R_exp);

            %- Combine the two selections
            ind_fit = ind_not_nan;% & ind_not_out;

            %- Fit only if enough data points
            if sum(ind_fit) > 4
                par_mod_loop    = par_mod;
                par_mod_loop{1} = time_ccc(ind_fit);            % For ccc

                %- Fit with different starting guess
                curves_fit = [];
                fit_summary_ccc = zeros(length(par_start_v),4);

                for i_Fit = 1:length(par_start_v)
                    par_start = par_start_v(i_Fit);
                    [par_fit R_fit bgd_perc ssr] = ccc_fit_05(R_exp(ind_fit),par_mod_loop,par_start,flagBGD,flagOutput);

                    fit_summary_ccc(i_Fit,1) = par_start;
                    fit_summary_ccc(i_Fit,2) = par_fit;
                    fit_summary_ccc(i_Fit,3) = ssr;
                    fit_summary_ccc(i_Fit,4) = bgd_perc;
                    curves_fit(:,i_Fit) = R_fit;        

                end

                fit_summary_all(iData).values     = fit_summary_ccc;
                fit_summary_all(iData).curves_exp = R_exp;
                fit_summary_all(iData).curves_fit = curves_fit;
                fit_summary_all(iData).ind_fit    = ind_fit;
            end  
        end
        

        
    end
    

    
%- Without parallel computing 
else
    for iData = 1:nData
        disp(' ')
        display(['Fit data-set ',num2str(iData),'/',num2str(nData)])        
        
        %- Get data
        R_exp = [];
        if orient_data == 2
           R_exp = R_ccc_lin(:,iData);
        elseif orient_data ==1
           R_exp = R_ccc_lin(iData,:);
        end

        %- Check is necessary to catch turn-over which couldn't be
        %  determined. All values are zero on log 2 scale - 1 in linear
        if not(sum(R_exp==1) == length(R_exp))
         
         
            %- Check for NAN and leave them out!
            ind_not_nan = ~isnan(R_exp);

            %- Check for outliers
            %ind_not_out = (R_exp<val_max) & (R_exp>val_min);

            %- Combine the two selections
            ind_fit = ind_not_nan;% & ind_not_out;

            %- Fit only if enough data points
            if sum(ind_fit) > 4
                par_mod_loop    = par_mod;
                par_mod_loop{1} = time_ccc(ind_fit);            % For ccc

                %- Fit with different starting guess
                curves_fit = [];
                fit_summary_ccc = zeros(length(par_start_v),4);

                for i_Fit = 1:length(par_start_v)
                     par_start = par_start_v(i_Fit);
                    [par_fit R_fit bgd_perc ssr] = ccc_fit_05(R_exp(ind_fit),par_mod_loop,par_start,flagBGD,flagOutput);

                    fit_summary_ccc(i_Fit,1) = par_start;
                    fit_summary_ccc(i_Fit,2) = par_fit;
                    fit_summary_ccc(i_Fit,3) = ssr;
                    fit_summary_ccc(i_Fit,4) = bgd_perc;
                    curves_fit(:,i_Fit) = R_fit; 
                end

                fit_summary_all(iData).values     = fit_summary_ccc;
                fit_summary_all(iData).curves_exp = R_exp;
                fit_summary_all(iData).curves_fit = curves_fit;
                fit_summary_all(iData).ind_fit    = ind_fit;
            end     
        end
    end
end


%% Pick the best fit from the different starting guesses

fit_summary_all_min = [];
data_fit_all = [];
data_fit_no_datapoints = [];
data_fit = [];

for iData = 1:size(fit_summary_all,2)
    dum1a = R_ccc_lin(iData,:);
    dum2a = zeros(size(dum1a));
    
    if ~isempty(fit_summary_all(iData).values)
        [C,ind_min] = min(fit_summary_all(iData).values(:,3));          
        fit_summary_all_min(iData,:) = fit_summary_all(iData).values(ind_min,1:4);
        
        %- Get the data - in the fits some of the data points are ignored
        ind_fit = fit_summary_all(iData).ind_fit;            

        dum1b           = zeros(size(dum1a));
        dum1b(ind_fit)  = dum1a(ind_fit);
        dum1b(~ind_fit) = NaN;
        dum1a = dum1b;
        
        dum2b = fit_summary_all(iData).curves_fit(:,ind_min)';
        dum2a(ind_fit) = dum2b;        
        dum2a(~ind_fit) = -999;        
    
        data_fit_no_datapoints(iData,1) = sum(ind_fit);
    
    end
    
   data_fit(iData,:) = dum2a;
   data_fit_all(iData,:) = [dum1a dum2a];   
end



%% Plot results
if flag_struct.plot
    
    for ind_site = 1:nData

        figure, hold on
        plot(time_ccc/60,R_ccc_lin(ind_site,:),'-ob')
        plot(time_ccc/60,data_fit(ind_site,:),'r')
        %plot(time_prot/60,prot_abs,'k')
        hold off
        box on
        xlabel('Time [min]') 
        ylabel('Turn-over ratio / protein level') 
        v= axis;
        axis([time_ccc(1)/60 time_ccc(end)/60 v(3) v(4)])
        legend('Turn-over: data', ['Fit: RT = ',num2str(round(1/fit_summary_all_min(ind_site,2))),'s'],'Protein level',4);    

    end
end
