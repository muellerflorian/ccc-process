function CCC_save_workspace_v1(file_name_full,handles)
% Function to write the results of the mapping to a text file

current_dir = pwd;

%- Ask for file-name if it's not specified
if isempty(file_name_full)
    
    if isfield(handles,'sites_path')
        cd(handles.sites_path)
        [pathstr, name, ext] = fileparts(handles.sites_file);
    else
        name='';
    end
        
    
    %- Ask user for file-name   
    file_name_default = [name,'__ANALYSIS_', datestr(now,1),'.mat'];
    [file_save,path_save] = uiputfile(file_name_default,'Save results of analysis [mat file]');
    file_name_full = fullfile(path_save,file_save);
    
else   
    file_save = 1;
end


%==== Save information of sites

if file_save ~= 0
    eval('save(file_name_full,''handles'',''-v6'')')
end

cd(current_dir)