function handles_GUI = CCC_load_workspace_v2(handles_GUI)
% Function to read analysis results from .mat file

    
%- Ask user for file-name for spot results
[file_load,path_load] =  uigetfile('*.mat','Results of analysis [mat file]');

if file_load ~= 0

    load(fullfile(path_load,file_load));

    %handles = evalin('base','handles');

    %== Load sites
    handles_GUI.status_sites = handles.status_sites;
    if handles_GUI.status_sites
        handles_GUI.sites      = handles.sites;
        handles_GUI.N_sites    = handles.N_sites;
        handles_GUI.sites_path = handles.sites_path;
        handles_GUI.sites_file = handles.sites_file;  
        
        if not(isfield(handles_GUI.sites,'TO_fit_ok'))
            [handles_GUI.sites.TO_fit_ok] = deal(1);
        end
        
        if not(isfield(handles_GUI.sites,'TO_fit_comment'))
            [handles_GUI.sites.TO_fit_comment] = deal('');
        end
        
        
    end

    %==== Load turn-over
    handles_GUI.status_TO = handles.status_TO;
    if handles_GUI.status_TO
        handles_GUI.time               = handles.time;
        handles_GUI.turn_over__values  = handles.turn_over__values;
        handles_GUI.turn_over__chr     = handles.turn_over__chr;
        handles_GUI.turn_over__pos_min = handles.turn_over__pos_min;
        handles_GUI.turn_over__pos_max = handles.turn_over__pos_max;
        handles_GUI.turn_over__pos     = handles.turn_over__pos;
        handles_GUI.turn_over__height  = handles.turn_over__height;  
        
        if not(isfield(handles_GUI,'N_T'))
            handles_GUI.N_T  = length(handles_GUI.time);
           
            scale = linspace(0,0.6,handles_GUI.N_T)';
            handles_GUI.color_time = repmat(scale,1,3); 
        end
        
    end

    %==== Load turn-over
    handles_GUI.status_mapped = handles.status_mapped;
    if handles_GUI.status_mapped
        handles_GUI.R_ccc_log  = handles.R_ccc_log; 
    end

    %== Load protein level
    handles_GUI.status_protein = handles.status_protein;
    if handles_GUI.status_protein
        handles_GUI.time_prot      = handles.time_prot;
        handles_GUI.prot_level_rel = handles.prot_level_rel;   
    end

    %== Load results of fit
    handles_GUI.status_fit = handles.status_fit;
    if handles_GUI.status_fit
        handles_GUI.fit_summary      = handles.fit_summary;
        handles_GUI.turn_over_fit    = handles.turn_over_fit;   
    end
end
