function file_write_fitting_par_v2(file_name_full,handles)
% Function to write the results of the mapping to a text file

current_dir = pwd;

%- Ask for file-name if it's not specified
if isempty(file_name_full)
    
    cd(handles.sites_path)
    
    [pathstr, name, ext] = fileparts(handles.sites_file);    
   
    %- Ask user for file-name
    file_name_default = [name,'__FIT_par.txt'];
    [file_save,path_save] = uiputfile(file_name_default,'Save results of fitting [Parameters]');
    file_name_full = fullfile(path_save,file_save);
    
else   
    file_save = 1;
end




%- Write to file
if file_save ~= 0
    
    %- Compile information
    sites       = handles.sites;
    N_sites     = handles.N_sites;
    fit_summary = handles.fit_summary;
    
    
    %- Generate string to write out data
    string_description = 'Site_Chr\tSite_Pos\tMapped_Chr\tMapped_Pos\tVal_init\tVal_fit\tSSR\tBGD\tFIT_ok\tComment\n';
    string_write       = '%s\t%d\t%s\t%d\t%d\t%d\t%d\t%d\t%d\t%s\n';
    
    
    %-Write to file
    fid = fopen(file_name_full,'w');
        
    fprintf(fid,'Fitting of turn-over: estimated parameters. Performed on %s \n', date);
    fprintf(fid,string_description);
    
    for i_Site = 1:N_sites
        
        chr_iPeak  = sites(i_Site).chr;
        pos_iPeak  = round(sites(i_Site).pos);

        TO_chr     = sites(i_Site).TO_chr;    
        TO_pos     = round(sites(i_Site).TO_pos); 
        
        TO_fit_ok     = sites(i_Site).TO_fit_ok;    
        TO_fit_comment     = round(sites(i_Site).TO_fit_comment); 
        
        TO_fit_par = sites(i_Site).TO_fit_par; 
        if isempty(TO_fit_par)
            TO_fit_par = zeros(4,1);
        end  

        fprintf(fid,string_write ,chr_iPeak,pos_iPeak,TO_chr,TO_pos,TO_fit_par,TO_fit_ok,TO_fit_comment);
    end   
    fclose(fid);
end
    

cd(current_dir)
    

