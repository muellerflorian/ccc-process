function [R_ccc_bgd_corr bgd_perc] =  ccc_batch_corr_bgd_v1(parameters,options)
% SCRIPT TO CORRECT DATA FOR GENOMIC BGD
%
%
% INPUT
%    time_prot      ... Time-points in seconds; should be a column vector (size(time_prot,2)=1) 
%    prot_level_rel ... Relative protein levels; again a column vector
%
%    time_ccc       ...  Time-points in seconds, don't have to be the same ones as for the protein levels
%                        Can be a row vector (like in the examples in the Excel file)
%                     
%    R_ccc_lin      .... Turn-over ratio for different promoter - linear scale and not log scale!
                     %   - Each promoter is in one row
                     %   - Columns correspond to time-points
                     % See Excel file 'Colin_ccc_data.xls' and sheet
                     % 'Data_ccc' for an example of two different
                     % promoters. 
%
% OUTPUT
%  R_ccc_bgd_corr  .... matrix: each row corresponds to one site and lists 
%                     the turnover value after bakground correction
%
% Florian Mueller, muellerf.research@gmail.com
%  - National Institutes of Health, LRBGE
%  - Institut Pasteur, Computational Imaging and Modeling Group


%% Some parameters for the processing
R_ccc_lin      = parameters.R_ccc_lin; 

flag_struct    = options.flag_struct; 
flagBGD       = flag_struct.flagBGD;                         

%% Fit all data
orient_data = 1;                % Orientation of Competition chip-data (1 in rows, 2 in columns)

if orient_data == 2
    nData = size(R_ccc_lin,2);
elseif orient_data ==1
    nData = size(R_ccc_lin,1);
end

  
%-- Loop over data
R_ccc_bgd_corr = [];
bgd_perc_corr  = [];

for iData = 1:nData 

    %display(['Correct data-set ',num2str(iData)])

    %- Get data
    R_exp = [];
    
    if orient_data == 2
        R_exp = R_ccc_lin(:,iData);
    elseif orient_data ==1
        R_exp = R_ccc_lin(iData,:);
    end

    %- Check is necessary to catch turn-over which couldn't be
    %  determined. All values are zero on log 2 scale - 1 in linear
    if not(sum(R_exp==1) == length(R_exp))


        %- Check for NAN and leave them out!
        ind_not_nan = ~isnan(R_exp);

        %- Combine the two selections
        ind_fit = ind_not_nan;% & ind_not_out;

        %- Fit only if enough data points
        if sum(ind_fit) > 4
             
            %- Get background value of site
            if flagBGD
                bgd_avg  = mean(R_exp(1:flagBGD));
                bgd_perc = 2*bgd_avg/(1+bgd_avg);
            else
                bgd_perc = 0;
            end
            
            if bgd_perc < 0
                bgd_perc = 0;
            end
                        
            %- Correct data
            p = (bgd_perc/(1-bgd_perc))/2;                 % Background as relative percentage (percent of IP)
            R_corr = (p*(1-R_exp) - R_exp ) ./ (p*(R_exp-1) -1);
            
            %- Save data
            bgd_perc_corr(iData,1) = bgd_perc;
            
            if orient_data == 2
                 R_ccc_bgd_corr(:,iData) = R_corr;
             elseif orient_data ==1
                 R_ccc_bgd_corr(iData,:) = R_corr;
            end
        end
    end
end
    

