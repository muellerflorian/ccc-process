function [parFit R_fit bgd_perc ssr] = ccc_fit_05(R_exp,par_mod,par_start,flagBGD,flagOutput)
% Fits experimental data from competition chip experiments
% and considers genomic background - genomic background is calculated from
% the first data point(s).
%
% flagBGD  ... Flag to determine if BGD is calculated or not
%              Serves also as an indicator of how many points at the
%              beginning should be averaged to calculate the background.

%- R_exp has to be a column vector
if size(R_exp,1)==1
    R_exp = R_exp';
end

%- Add background to parameter list
if flagBGD
    bgd_avg = mean(R_exp(1:flagBGD));
    bgd_perc = 2*bgd_avg/(1+bgd_avg);
    if bgd_perc < 0
        bgd_perc = 0;
    end
else
    bgd_perc = 0;
end    
par_mod{5} = bgd_perc;

%- Set-up of fitting routine
lb = 0;
ub = 1000;

options = optimset('Display','off');
[parFit, ssr] = lsqcurvefit(@ratio_fun, par_start, par_mod, R_exp,lb,ub,options);
R_fit = ratio_fun(parFit,par_mod);


if flagOutput
    figure, hold on
    plot(par_mod{1},R_exp,'ok')
    plot(par_mod{1},R_fit,'r')
    hold off
    box on
    xlabel('Time (min)')
    title(['Best fit: a = ',num2str(parFit)]) 
    legend('Data',['Fit. ssr = ',num2str(ssr),', \alpha = ',num2str(parFit)],4)
   
end

%-- Solve differential equation with Runge Kutta
function R_calc_bgd = ratio_fun(parFit,par_mod)
time      = par_mod{1};
time_prot = par_mod{2};
prot_rel  = par_mod{3};
IC        = par_mod{4};
bgd_p     = par_mod{5};

[T,P] = ode45(@(t, y) dy_fun(t,y,prot_rel,time_prot,parFit),time,IC);
R_calc     = (P./(1-P));

%- Consider background
p = (bgd_p/(1-bgd_p))/2;                 % Background as relative percentage (percent of IP)
R_calc_bgd = (R_calc + p*(R_calc+1))./(1 + p*(R_calc+1));


%-- Function for ode45 (Runge-Kutta)
% see Matlab help for more details
function dy = dy_fun(t,y,RT,time,parFit)

%- Interpolate only if necessary
ind_t = find(time==t);

if isempty(ind_t)
    R = interp1(time,RT,t);         % Interpolate the data-set (RT,time) at time t
else
    R = RT(ind_t);
end

alpha = parFit(1);

dy = alpha*( R - y);





