function file_write_vgd_corr_v1(file_name_full,handles)
% Function to write the results of the mapping to a text file

current_dir = pwd;

%- Ask for file-name if it's not specified
if isempty(file_name_full)
    
    cd(handles.sites_path)
    [pathstr, name, ext] = fileparts(handles.sites_file);    
   
    %- Ask user for file-name
    file_name_default = [name,'__BGD_CORR.txt'];
    [file_save,path_save] = uiputfile(file_name_default,'Save results of bgd correction');
    file_name_full = fullfile(path_save,file_save);
    
else   
    file_save = 1;
end




%- Write to file
if file_save ~= 0
        
    %- Compile information
    sites      = handles.sites;
    N_sites    = handles.N_sites;
    R_ccc_corr = handles.R_ccc_bgd_corr;
    
    N_T = size(R_ccc_corr,2);
    
    
    %- Generate string to write out data
    string_description = 'Site_Chr\tSite_Pos\tMapped_Chr\tMapped_Pos';
    string_write       = '%s\t%d\t%s\t%d';
    
    for i_T = 1:N_T
       str_dum1 = ['\t',num2str(handles.time(i_T)/60)];
       string_description = [string_description,str_dum1]; 
       
       str_dum2 = '\t%f';
       string_write = [string_write,str_dum2];
    end
    string_description = [string_description,'\n']; 
    string_write       = [string_write,'\n']; 
    
    
    %-Write to file
    fid = fopen(file_name_full,'w');
        
    fprintf(fid,'Correct turnover data for genomic background. Performed on %s \n', date);
    fprintf(fid,string_description);
    
    for i_Site = 1:N_sites
        
        chr_iPeak  = sites(i_Site).chr;
        pos_iPeak  = round(sites(i_Site).pos);

        TO_chr     = sites(i_Site).TO_chr;    
        TO_pos     = round(sites(i_Site).TO_pos); 
        
        TO_val = R_ccc_corr(i_Site,:);
        
        if isempty(TO_val)
            TO_val = zeros(N_T,1);
        end  

        fprintf(fid,string_write ,chr_iPeak,pos_iPeak,TO_chr,TO_pos,TO_val);
    end      
    
    fclose(fid);
end
    

cd(current_dir)
    
