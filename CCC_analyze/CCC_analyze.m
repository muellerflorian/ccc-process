function varargout = CCC_analyze(varargin)
% CCC_ANALYZE M-file for CCC_analyze.fig
%      CCC_ANALYZE, by itself, creates a new CCC_ANALYZE or raises the existing
%      singleton*.
%
%      H = CCC_ANALYZE returns the handle to a new CCC_ANALYZE or the handle to
%      the existing singleton*.
%
%      CCC_ANALYZE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CCC_ANALYZE.M with the given input arguments.
%
%      CCC_ANALYZE('Property','Value',...) creates a new CCC_ANALYZE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CCC_analyze_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CCC_analyze_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CCC_analyze

% Last Modified by GUIDE v2.5 24-Jan-2012 14:13:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CCC_analyze_OpeningFcn, ...
                   'gui_OutputFcn',  @CCC_analyze_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CCC_analyze is made visible.
function CCC_analyze_OpeningFcn(hObject, eventdata, handles, varargin)

%- Read-in data
if not(isempty(varargin))
    handles.sites             = varargin{1};
    handles.turn_over__values = varargin{2};
    handles.time              = varargin{3};
    
    handles.status_TO      = 1;
    handles.status_sites   = 1;
    
    handles.N_sites =  length(handles.sites);
    site_plot(hObject, eventdata, handles)
else
    handles.sites             = {};
    handles.turn_over__values = {}; 
    handles.status_TO         = 0;
    handles.status_sites      = 0;
    handles.time = []; 
end

%- Set some flags
handles.status_mapped  = 0;
handles.status_protein = 0;
handles.status_fit     = 0;
handles.status_corr_bgd = 0;

%- Clear axis
set(handles.axes_1,'Visible','off')
set(handles.axes_2,'Visible','off')
set(handles.axes_3,'Visible','off')

%- Default parameters
handles.d_offset_max   = 1000;
handles.d_reg_extract  = 3500;
handles.d_TO_avg       = 150;
handles.flag_match     = 0;     % 1 ... based on occupancy data
                                % 2 ... based on averaged turn-over (over time)

                                % Choose default command line output for CCC_analyze

handles.flagBGD  = 4;
handles.output = hObject;


% Update handles structure
enable_controls(hObject, eventdata, handles)
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = CCC_analyze_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


%=== Function to enable controls based on current status of analysis
function enable_controls(hObject, eventdata, handles)


%- Activate mapping
if handles.status_TO && handles.status_sites
    set(handles.button_map_sites,'Enable','on');    
else
    set(handles.button_map_sites,'Enable','off');  
end


%- After mapping is done
if  handles.status_mapped
    set(handles.save_mapping,'Enable','on'); 
    
    set(handles.axes_1,'Visible','on')
    set(handles.axes_2,'Visible','on') 
else
    set(handles.save_mapping,'Enable','off'); 
    
    cla(handles.axes_1); cla(handles.axes_2);
    set(handles.axes_1,'Visible','off')
    set(handles.axes_2,'Visible','off')
end

%- Activate fitting
if  handles.status_mapped && handles.status_protein
    set(handles.button_fit,'Enable','on');    
else
    set(handles.button_fit,'Enable','off');  
end


%- After fitting is done
if  handles.status_fit
    set(handles.save_fit_par,'Enable','on'); 
    set(handles.save_fit_curves,'Enable','on');
    set(handles.check_fit_good,'Enable','on');   
    
    set(handles.axes_3,'Visible','on')
else
    set(handles.save_fit_par,'Enable','off');  
    set(handles.save_fit_curves,'Enable','off'); 
    set(handles.check_fit_good,'Enable','off'); 
    
    cla(handles.axes_3);
    set(handles.axes_3,'Visible','off')
end


%- After correction for background
if  handles.status_corr_bgd
    set(handles.menu_correct_bgd_save,'Enable','on');    
else
    set(handles.menu_correct_bgd_save,'Enable','off');  
end




%==== FUNCTIONs with function
function menu_options_Callback(hObject, eventdata, handles)

%- Get string of time-points
time          = handles.time;
if not(isempty(time))
    time_str      = num2str((time/60)','%d,');      % Convert to minutes and then to a string
    time_str(end) = [];                             % Delete the last ,
else
    time_str ='';
end

%- Setup dialog
dlg_title = 'Preferences';
prompt{1} = 'Maximum offset to match peaks:';
prompt{2} = 'Size of extracted region around peak [+-/ bp]:';
prompt{3} = 'Size of region to average turn-over  [+-/ bp]:';
prompt{4} = 'Site matching: 0:occupancy, 1-N:time-avged turn-over';
prompt{5} = 'Time, separated by , [min]:';
prompt{6} = 'Number of time-points to average beginning:';


def{1} = num2str(handles.d_offset_max);
def{2} = num2str(handles.d_reg_extract);
def{3} = num2str(handles.d_TO_avg);
def{4} = num2str(handles.flag_match);
def{5} = time_str;
def{6} = num2str(handles.flagBGD);

num_lines = 1;
answer = inputdlg(prompt,dlg_title,num_lines,def);

if not(isempty(answer))
    handles.d_offset_max  = str2double(answer{1});
    handles.d_reg_extract = str2double(answer{2});
    handles.d_TO_avg      = str2double(answer{3});
    handles.flag_match    = str2double(answer{4}); 
    handles.flagBGD       = str2double(answer{6}); 
    
    %- Analyze time
    time_str              = answer{5};
    time                  = str2num(time_str)'*60;
    handles.time = time;    
end

enable_controls(hObject, eventdata, handles)
guidata(hObject, handles);


%========================================================================== 
% LOAD AND PROCESS TURNOVER DATA
%========================================================================== 

%=== Load turnover data
function load_turn_over_Callback(hObject, eventdata, handles)

status_update(hObject, eventdata, handles,{'Loading turn-over data ....'})
set(handles.figure1,'Pointer','watch');

[fileName_turn_over,pathName_turn_over] = uigetfile('.txt','Specify file with turn-over data') ;

if fileName_turn_over ~= 0
    fid = fopen(fullfile(pathName_turn_over,fileName_turn_over));

    %- Read-in header line and determine number of columns
    header_line = fgetl(fid);
    num_cols    = 1 + sum(header_line == sprintf('\t'));

    %- Read in data
    str_read_in = ['%s%d%d%f',repmat('%f', 1, num_cols-4)];
    turn_over_struct = textscan(fid, str_read_in,'HeaderLines',0,'delimiter','\t','CollectOutput',1);
    fclose(fid);

    %- Assign values
    turn_over__chr     = turn_over_struct{1};
    turn_over__pos_min = turn_over_struct{2}(:,1);
    turn_over__pos_max = turn_over_struct{2}(:,2);
    turn_over__pos     = (turn_over__pos_min + turn_over__pos_max)/2;

    turn_over__height  = turn_over_struct{3}(:,1);
    turn_over__values  = turn_over_struct{3}(:,2:end);

    %- Analyze header to get time-point
    ind_tab  = strfind(header_line,sprintf('\t'));
    time_str = header_line(ind_tab(4):end);
    time     = str2num(time_str)'*60;
    N_T       = length(time);
    
    %- Gray-scales to color code time
    scale = linspace(0,0.6,N_T)';
    color_time = repmat(scale,1,3);        

    %- Assing to handles structure
    handles.turn_over__values = turn_over__values;

    handles.turn_over__chr    = turn_over__chr;
    handles.turn_over__pos_min = turn_over__pos_min;
    handles.turn_over__pos_max = turn_over__pos_max;
    handles.turn_over__pos     = turn_over__pos;
    handles.turn_over__height  = turn_over__height; 

    handles.time = time;
    handles.N_T  = N_T;
    handles.color_time = color_time;   
    

    handles.status_TO = 1;
    enable_controls(hObject, eventdata, handles)
    guidata(hObject, handles);
    status_update(hObject, eventdata, handles,{'Loading turn-over data FINISHED'})
end
set(handles.figure1,'Pointer','arrow');


%=== Load data with site description
function button_load_sites_Callback(hObject, eventdata, handles)

status_update(hObject, eventdata, handles,{'Loading site definition ....'})
set(handles.figure1,'Pointer','watch');
[fileName_peaks,pathName_peaks] = uigetfile('.txt','Specify file with peak locations') ;

if fileName_peaks ~= 0

    fid = fopen(fullfile(pathName_peaks,fileName_peaks));
    
    
    %- Read-in header line and determine number of columns
    header_line = fgetl(fid);
    num_cols    = 1 + sum(header_line == sprintf('\t'));

    if num_cols == 3
        handles.flag_sites = 'center';
        
        site_struct = textscan(fid, '%s %f %s','HeaderLines',0,'delimiter','\t');
        fclose(fid);

        sites_chr     = site_struct{1};
        sites_pos     = site_struct{2};
        sites_comment = site_struct{3};

        %- Save results in structure
        N_sites = size(sites_chr,1);
        sites = {};
        for i = 1:N_sites    
            sites(i).chr     = sites_chr{i};
            sites(i).pos     = sites_pos(i);
            sites(i).comment = sites_comment(i);
        end
        
    elseif num_cols == 4
       handles.flag_sites = 'start_stop';
        site_struct = textscan(fid, '%s %f %f %s','HeaderLines',0,'delimiter','\t');
        fclose(fid);

        sites_chr     = site_struct{1};
        sites_start   = site_struct{2};
        sites_stop     = site_struct{3};
        sites_comment = site_struct{4};

        %- Save results in structure
        N_sites = size(sites_chr,1);
        sites = {};
        for i = 1:N_sites    
            sites(i).chr     = sites_chr{i};
            sites(i).start   = sites_start(i);
            sites(i).stop     = sites_stop(i);
            sites(i).pos  = (sites_start(i) + sites_stop(i)) /2;
            sites(i).comment = sites_comment(i);
            
        end
        
    end
    handles.sites        = sites;
    handles.N_sites      = N_sites;
    handles.status_sites = 1;
    handles.status_fit = 0;
    handles.status_mapped = 0;
    enable_controls(hObject, eventdata, handles)

    handles.sites_path = pathName_peaks;
    handles.sites_file = fileName_peaks;

    guidata(hObject, handles);
    status_update(hObject, eventdata, handles,{'Loading site definition FINISHED'})
end
set(handles.figure1,'Pointer','arrow');

%==== MAP sites and extract turn-over
function button_map_sites_Callback(hObject, eventdata, handles)

status_update(hObject, eventdata, handles,{'Mapping sites ...'})
set(handles.figure1,'Pointer','watch');

%- Important parameters for analysis
d_offset_max  = handles.d_offset_max;
d_reg_extract = handles.d_reg_extract;
d_TO_avg      = handles.d_TO_avg;
flag_match    = handles.flag_match; 


%- Get data
sites = handles.sites;

turn_over__chr      = handles.turn_over__chr;
turn_over__pos      = handles.turn_over__pos;
turn_over__height   = handles.turn_over__height; 
N_sites             = handles.N_sites;
turn_over__values   = handles.turn_over__values; 

%- Loop over sites
ind_region_TO_all = [];
occ_all           = [];

for i_Site = 1:N_sites
     
    disp(['Mapping site: ', num2str(i_Site) , ' of ', num2str(N_sites)])
    
    % Find all probes on same chromosome
    chr_iPeak  = sites(i_Site).chr;
    ind_ch     = find(strcmp(chr_iPeak, turn_over__chr)); 
    
    % Find position of probes
    switch handles.flag_sites
        
        case 'center'
            pos_iPeak  = sites(i_Site).pos;
            
            if d_offset_max > 0

                
                
                ind_pos_lb = find(turn_over__pos > (pos_iPeak-d_offset_max));
                ind_pos_ub = find(turn_over__pos < (pos_iPeak+d_offset_max));
                
                ind_Peak =  intersect(intersect(ind_ch, ind_pos_lb),ind_pos_ub);
                
            else
                dist_abs = abs(turn_over__pos-pos_iPeak);
                
                [min_dist ind_min_rel] = min(dist_abs(ind_ch));
                ind_Peak    = ind_ch(ind_min_rel(1));
            end
        
        
        case 'start_stop'
            start_iPeak = sites(i_Site).start;
            stop_iPeak  = sites(i_Site).stop;
            
            ind_pos_lb = find(turn_over__pos > (start_iPeak));
            ind_pos_ub = find(turn_over__pos < (stop_iPeak));

            ind_Peak =  intersect(intersect(ind_ch, ind_pos_lb),ind_pos_ub);
    end
    
    
    %- Choose maximum value
    if ~isempty(ind_Peak)
        
        %=== Find location of maximum
        
        if      flag_match == 0
            TO_map_int = turn_over__height(ind_Peak);
        
        else  
            TO_map_int = 1./mean(turn_over__values(ind_Peak,1:flag_match),2); 
        end
        
        [peak_max_loop,ind_max_loop] = max(TO_map_int);   
        ind_Peak_MAX = ind_Peak(ind_max_loop);
        
        
        %== Save information
        sites(i_Site).TO_occ_log2   =  peak_max_loop;
        sites(i_Site).TO_ind        =  ind_Peak_MAX;  
        
        sites(i_Site).TO_chr    = turn_over__chr{ind_Peak_MAX};
        sites(i_Site).TO_pos    = turn_over__pos(ind_Peak_MAX); 
        sites(i_Site).TO_height = turn_over__height(ind_Peak_MAX); 
        
        sites(i_Site).TO_map_int = TO_map_int;        
        sites(i_Site).TO_map_pos = turn_over__pos(ind_Peak); 
        
        
        %=== Extract turn-over by averaging a certain range at this site
        switch handles.flag_sites
        
            case 'center'
                ind_lb = find(turn_over__pos > (sites(i_Site).TO_pos - d_TO_avg));
                ind_ub = find(turn_over__pos < (sites(i_Site).TO_pos + d_TO_avg));
                
            case 'start_stop'
                ind_lb = ind_pos_lb;
                ind_ub = ind_pos_ub;
                
        end
        
        ind_region_TO           = intersect(intersect(ind_ch, ind_lb),ind_ub);
        R_ccc_log(i_Site,:)     = mean(turn_over__values(ind_region_TO,:),1);
        
        sites(i_Site).TO_values     = R_ccc_log(i_Site,:);
        sites(i_Site).TO_region_avg = turn_over__pos(ind_region_TO);            
       
        %=== Extract the turn-over at a region around 
        ind_lb = find(turn_over__pos > (sites(i_Site).TO_pos-d_reg_extract));
        ind_ub = find(turn_over__pos < (sites(i_Site).TO_pos+d_reg_extract));
  
        ind_region = intersect(intersect(ind_ch, ind_lb),ind_ub);
        
        sites(i_Site).TO_region     = turn_over__values(ind_region,:);
        sites(i_Site).TO_region_pos = turn_over__pos(ind_region);
        
        %== Save indices of peaks and their occupancy
       % ind_region_TO_all = [ind_region_TO_all;ind_region_TO];
        
       % occ_loop = peak_max_loop* ones(size(ind_region_TO));
       % occ_all  = [occ_all; occ_loop];
    else 
        R_ccc_log(i_Site,:) = zeros(1,handles.N_T );
        disp(['NOT MAPPED'])
    end
        
end

handles.sites = sites;
handles.R_ccc_log = R_ccc_log;
handles.status_mapped = 1;
site_plot(hObject, eventdata, handles)

enable_controls(hObject, eventdata, handles)
guidata(hObject, handles);
status_update(hObject, eventdata, handles,{'Mapping sites FINISHED'})
set(handles.figure1,'Pointer','arrow');

%========================================================================== 
% Fit turnover data
%========================================================================== 

%=== Load protein levels
function button_load_protein_Callback(hObject, eventdata, handles)

[time_prot prot_level_rel status_protein] = file_load_protein_v1([]);

if status_protein
    handles.time_prot       = time_prot;
    handles.prot_level_rel  = prot_level_rel;
    handles.status_protein  = 1;
end

enable_controls(hObject, eventdata, handles)
guidata(hObject, handles);


%=== Function to fit all turn-over
function button_fit_Callback(hObject, eventdata, handles)

status_update(hObject, eventdata, handles,{'Fitting turn-over ...'})
options.flag_struct.flagBGD  = handles.flagBGD;
options.flag_struct.plot     = 0;
options.flag_struct.parallel = get(handles.flag_parallel,'Value');       % Utilizes parallel toolbox for processing

%- Get turn-over an convert to linear
R_ccc_log = handles.R_ccc_log;

parameters.time_ccc       = handles.time;
parameters.R_ccc_lin      = 2.^R_ccc_log;
parameters.time_prot      = handles.time_prot;
parameters.prot_level_rel = handles.prot_level_rel;

set(handles.figure1,'Pointer','watch');
status_update(hObject, eventdata, handles,{'FITTING TURNOVER DATA ... please wait ....'})

[handles.fit_summary handles.turn_over_fit] = ccc_batch_fit_bgd_06(parameters,options);

set(handles.figure1,'Pointer','arrow');
status_update(hObject, eventdata, handles,{'DATA FITTING FINISHED!'})

%- Summarize results
sites   = handles.sites;
N_sites = handles.N_sites;

for i_Site = 1:N_sites
    sites(i_Site).TO_fit_lin     = handles.turn_over_fit(i_Site,:);
    sites(i_Site).TO_fit_log2    = log2(handles.turn_over_fit(i_Site,:));
    sites(i_Site).TO_fit_par     = handles.fit_summary(i_Site,:);
    sites(i_Site).TO_fit_ok      = 1;
    sites(i_Site).TO_fit_comment = '';
end
    
handles.sites      = sites;
handles.status_fit = 1;
enable_controls(hObject, eventdata, handles)
guidata(hObject, handles);
status_update(hObject, eventdata, handles,{'Fitting turn-over FINSISHED'})


%=== Plot results
function site_plot(hObject, eventdata, handles)

if handles.status_mapped

    %= Get number of site
    ind_site = str2double(get(handles.text_site_no,'String')); 

    %- Only if there are sites defined
    if not(isempty(handles.sites))
        site_fun = handles.sites(ind_site);

        %== Indicate position of site
        txt_pos = [site_fun.chr,': ', num2str(site_fun.pos)];
        set(handles.text_site_descript,'String',txt_pos); 

        %= Comment of site
        set(handles.text_site_comment,'String',site_fun.comment);

        %= Plot turn-over as function of genomic distance
        pos_plot_reg  = double(site_fun.TO_region_pos) - double(site_fun.TO_pos);
        pos_plot_site = double(site_fun.pos)           - double(site_fun.TO_pos);
        %d_TO_avg      = handles.d_TO_avg;
        region_avg     = double(site_fun.TO_region_avg) - double(site_fun.TO_pos);

        if not(isempty(pos_plot_site))    
            axes(handles.axes_1)
            cla
            plot(pos_plot_reg,site_fun.TO_region)
             v1 = axis;

             hold all
                 h(1) = area([region_avg(1), +region_avg(end)],[100 100]);        
                 set(h(1),'FaceColor',[.7 .7 .7])
                 set(h(1),'EdgeColor',[.7 .7 .7])
                 set(h(1),'BaseValue',-100)
                 alpha(0.5)

                 plot([pos_plot_site, pos_plot_site],[-100,100],'-k')
             hold off
             axis(v1)
             box on
             xlabel('Distance 1 [bp]')
             ylabel('Turn-over ratio [log2]')
        else
            cla(handles.axes_1)
        end

        %= Metric that was used to map site
        TO_map_int = site_fun.TO_map_int;
        TO_map_pos = double(site_fun.TO_map_pos)-double(site_fun.TO_pos);

        if not(isempty(pos_plot_site)) 
            axes(handles.axes_3)
            plot(TO_map_pos,TO_map_int)
            v2 = axis;
            hold all
                plot([pos_plot_site, pos_plot_site],[-100,100],'-k')        
            hold off
            axis([v1(1) v1(2) v2(3) v2(4)]) ;
            xlabel('Distance [bp]')
            ylabel('Occupancy [log2]')
        else
            cla(handles.axes_3)
        end

        %= Update manual quality check
        if handles.status_fit  
            set(handles.check_fit_good,'Value',site_fun.TO_fit_ok);
            set(handles.text_comment,'String',site_fun.TO_fit_comment);  
        end

        %= Plot turn-over at this site
        flag_scale = get(handles.button_scale_ratio,'Value');

        if not(isempty(site_fun.TO_values))



            %= Plot fits
            axes(handles.axes_2)
            if flag_scale == 1
                plot(handles.time/60,site_fun.TO_values)

                if isfield(site_fun,'TO_fit_log2')
                    hold all
                        plot(handles.time/60,site_fun.TO_fit_log2,'r')
                     hold off
                     box on
                     xlabel('Time [min]')
                     ylabel('Turn-over ratio [log2]')
                end

            else
                plot(handles.time/60,2.^site_fun.TO_values)
                if isfield(site_fun,'TO_fit_lin')
                    hold all
                    plot(handles.time/60,site_fun.TO_fit_lin,'r')            
                    hold off
                    box on
                    xlabel('Time [min]')
                    ylabel('Turn-over ratio [linear]')
                end

            end


            if handles.status_fit
                %site_fun.TO_fit_par
                RT = round(1/site_fun.TO_fit_par(2));
                set(handles.text_residence_time,'String',num2str(RT));
            else
                set(handles.text_residence_time,'String',' ');
            end

        else
            cla(handles.axes_2)
        end
    end
else
    cla(handles.axes_1); cla(handles.axes_2);cla(handles.axes_3);
    set(handles.axes_1,'Visible','off')
    set(handles.axes_2,'Visible','off')
    set(handles.axes_3,'Visible','off')
end
    

%== Correct data for genomic background 
function menu_correct_bgd_Callback(hObject, eventdata, handles)
%- Get turn-over an convert to linear
R_ccc_log = handles.R_ccc_log;
parameters.R_ccc_lin         = 2.^R_ccc_log;
options.flag_struct.flagBGD  = handles.flagBGD;

[R_ccc_bgd_corr bgd_perc] =  ccc_batch_corr_bgd_v1(parameters,options);

%- Save results and update status
handles.R_ccc_bgd_corr = R_ccc_bgd_corr;
handles.bgd_perc = bgd_perc;
handles.status_corr_bgd = 1;
enable_controls(hObject, eventdata, handles)
guidata(hObject, handles);
status_update(hObject, eventdata, handles,{'Background correction finished'})



%========================================================================== 
% Various functions for plotting
%========================================================================== 

%== Change number of site
function text_site_no_Callback(hObject, eventdata, handles)
ind_site = str2double(get(handles.text_site_no,'String')); 
slider_val = (ind_site-1)/(handles.N_sites-1);
set(handles.slider_site,'Value',slider_val);

site_plot(hObject, eventdata, handles)


%- Change slider
function slider_site_Callback(hObject, eventdata, handles)
slider_val = get(handles.slider_site,'Value');
ind_site   = round(slider_val*(handles.N_sites-1)) +1;

set(handles.text_site_no,'String',ind_site); 
site_plot(hObject, eventdata, handles)


%- Down one
function push_down_one_Callback(hObject, eventdata, handles)
ind_site = str2double(get(handles.text_site_no,'String')); 
ind_new  = (ind_site-1);
if ind_new < 1
    ind_new = 1;
end

set(handles.text_site_no,'String',num2str(ind_new));

slider_val = (ind_new-1)/(handles.N_sites-1);
set(handles.slider_site,'Value',slider_val);

site_plot(hObject, eventdata, handles)


%- Up one
function push_up_one_Callback(hObject, eventdata, handles)
ind_site = str2double(get(handles.text_site_no,'String')); 
ind_new  = (ind_site+1);
if ind_new > handles.N_sites
    ind_new = handles.N_sites;
end

set(handles.text_site_no,'String',num2str(ind_new));

slider_val = (ind_new-1)/(handles.N_sites-1);
set(handles.slider_site,'Value',slider_val);

site_plot(hObject, eventdata, handles)


%========================================================================== 
% Save and load results
%========================================================================== 


%== Write results of mapping
function save_mapping_Callback(hObject, eventdata, handles)
file_write_mapping_v2([],handles)


%== Write results of fitting: Parameters
function save_fit_par_Callback(hObject, eventdata, handles)
file_write_fitting_par_v2([],handles)


%== Write results of fitting: curves
function save_fit_curves_Callback(hObject, eventdata, handles)
file_write_fitting_curves_v2([],handles)


%== Save workspace 
function save_analysis_Callback(hObject, eventdata, handles)
CCC_save_workspace_v1([],handles)


%== Save workspace 
function load_analysis_Callback(hObject, eventdata, handles)
handles = CCC_load_workspace_v2(handles);
enable_controls(hObject, eventdata, handles)
site_plot(hObject, eventdata, handles)
guidata(hObject, handles);

%== Save data corrected for genomic background 
function menu_correct_bgd_save_Callback(hObject, eventdata, handles)

%figure, plot(handles.R_ccc_bgd_corr(1:5,:));
%handles.R_ccc_bgd_corr(1,:)

file_write_vgd_corr_v1([],handles)


%========================================================================== 
% MISC functions
%========================================================================== 

%== Update status
function status_update(hObject, eventdata, handles,status_text)
status_old = get(handles.list_box_status,'String');
status_new = [status_old;status_text];
set(handles.list_box_status,'String',status_new)
set(handles.list_box_status,'ListboxTop',round(size(status_new,1)))
drawnow
guidata(hObject, handles);


%== Update manual quality check of fit
function check_fit_good_Callback(hObject, eventdata, handles)

%= Get number of site
ind_site = str2double(get(handles.text_site_no,'String')); 
fit_ok   = get(handles.check_fit_good,'Value');
handles.sites(ind_site).TO_fit_ok =fit_ok; 
guidata(hObject, handles);


%== Comment for fit
function text_comment_Callback(hObject, eventdata, handles)
ind_site = str2double(get(handles.text_site_no,'String')); 
comment  = get(handles.text_comment,'String');
handles.sites(ind_site).TO_fit_comment =comment; 
guidata(hObject, handles);


%== Parallel toolbox
function flag_parallel_Callback(hObject, eventdata, handles)

flag_parallel = get(handles.flag_parallel,'Value');

if exist('matlabpool')

    %- Parallel computing - open MATLAB session for parallel computation 
    if flag_parallel == 1    
        isOpen = matlabpool('size') > 0;
        if (isOpen==0)

            set(handles.figure1,'Pointer','watch');
            status_update(hObject, eventdata, handles,{'Opening Matlab-pool ... please wait ....'})
            matlabpool open;  
            status_update(hObject, eventdata, handles,{' ... finished!'})
            set(handles.figure1,'Pointer','arrow');
        end

    %- Parallel computing - close MATLAB session for parallel computation     
    else
        isOpen = matlabpool('size') > 0;
        if (isOpen==1)
            
            set(handles.figure1,'Pointer','watch');
            status_update(hObject, eventdata, handles,{'Closing Matlab-pool ... please wait ....'})
            matlabpool close;
            status_update(hObject, eventdata, handles,{' ... finished!'})
            set(handles.figure1,'Pointer','arrow');
        end
    end
    
else
    warndlg('Parallel toolbox not available','FISH_QUANT')
    set(handles.flag_parallel,'Value',0);
end



%========================================================================== 
% FUNCTIONs without function
%========================================================================== 

function slider_site_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function text_site_no_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_site_descript_Callback(hObject, eventdata, handles)

function text_site_descript_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function button_scale_ratio_Callback(hObject, eventdata, handles)

function text_site_comment_Callback(hObject, eventdata, handles)

function text_site_comment_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function list_box_status_Callback(hObject, eventdata, handles)

function list_box_status_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Untitled_1_Callback(hObject, eventdata, handles)

function Untitled_3_Callback(hObject, eventdata, handles)

function Untitled_4_Callback(hObject, eventdata, handles)

function Untitled_5_Callback(hObject, eventdata, handles)

function text_residence_time_Callback(hObject, eventdata, handles)

function text_residence_time_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_comment_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function menu_correct_Callback(hObject, eventdata, handles)
