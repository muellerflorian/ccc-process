# ccc-process #



### What is this repository for? ###

Collection of Matlab tools to model and analyze data from competition chip experiments.


### How do I get set up? ###

For details on model and processing steps see also the publication: 

Lickwar CR, Mueller F, Lieb JD. Genome-wide measurement of protein-DNA binding dynamics using competition ChIP. Nature Protocols,  2013; 8(7): 1337-53

Lickwar CR, Mueller F, Hanlon S, Mcnally JG, Lieb JD. Genome-wide protein-DNA binding dynamics suggest a molecular clutch for transcription factor function. Nature, 2012; 484: 251-255 

Each of the module has a documentation in PDF format.
