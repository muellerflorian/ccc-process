function [results_norm] = CCC_lowess_apply_v1(results_preproc,path_name,par)

fprintf('\n===== LOWESS NORMALIZATION - follow status updates below \n');

%% Parallel computing - open MATLAB session for parallel computation 
if (par.flag_struct.parallel)
    isOpen = matlabpool('size') > 0;
    if (isOpen==0)
        matlabpool open;
    end
end


%% Get all parameters
ind_Rap1_not_targets = results_preproc.ind_Rap1_not_targets;
N_REPEATS_TOR        = results_preproc.N_REPEATS_TOR;
N_REPEATS_OCC        = results_preproc.N_REPEATS_OCC;
data_channel         = results_preproc.data_channel;
data_TOR             = results_preproc.data_TOR;
data_OCC             = results_preproc.data_OCC;
N_OCC                = results_preproc.N_OCC;

link_probes_genome_IND_in_turn_over = results_preproc.link_probes_genome_IND_in_turn_over ;
link_probes_genome_chr   = results_preproc.link_probes_genome_chr;
link_probes_genome_end   = results_preproc.link_probes_genome_end;
link_probes_genome_start = results_preproc.link_probes_genome_start;


%% Path-name to save plots
path_save = fullfile(path_name.channel,['_Plots_',datestr(now, 'yymmdd')]);

is_dir = exist(path_save,'dir'); 

if is_dir == 0
   mkdir(path_save)
end


%% ========================================================================
%  [1] LOWESS normalization: pairwise for all dye-swap experiments
%  ========================================================================

%- Some parameters
N_proc                         = length(ind_Rap1_not_targets);
parameters_LOWESS.window_size  = par.LOWESS.window_size;
flags_loop                     = par.flag_struct;

%====== LOOP OVER TIME_COURSE

fprintf('* Lowess normalization (Turnover) .... this now really takes a while .... \n')

if ( par.flag_struct.parallel )

    %-- Loop over replicates
    for i_Rep = 1:N_REPEATS_TOR 
      
        fprintf('\nProcessing replicate %d of %d \n', i_Rep, N_REPEATS_TOR);        
        fprintf('  - Processing %d time-points on %d CPUs. \n', par.N_T,matlabpool('size'));
        fprintf('  - Listed times below are for processing of one time-point by one CPU. \n');
   
        data_dum_FLAG_all = data_channel.FLAG(:,:,i_Rep);
        data_dum_MYC_all  = data_channel.MYC(:,:,i_Rep);
    
        data_dum_FLAG_affected = data_channel.FLAG(ind_Rap1_not_targets,:,i_Rep);
        data_dum_MYC_affected  = data_channel.MYC(ind_Rap1_not_targets,:,i_Rep);       
        
        
        
        parfor i_T = 1:par.N_T;
            tic
            parameters_LOWESS_loop = parameters_LOWESS;      
            time_str               = num2str(par.time_vect(i_T));      

            %- Select data
            data_dum = [];
            data_dum(:,1) = data_dum_FLAG_affected(:,i_T);
            data_dum(:,2) = data_dum_MYC_affected(:,i_T);
            data_ref      = data_dum(1:N_proc,:);

            data_all_loop = [];
            data_all_loop(:,1) = data_dum_FLAG_all(:,i_T);
            data_all_loop(:,2) = data_dum_MYC_all(:,i_T);
            
            %- Process
            parameters_LOWESS_loop.file_name_start = ['CCC_Lowess_Rep',num2str(i_Rep), '_T' ,time_str];
            parameters_LOWESS_loop.path_save       = path_save;
            
            [data_all_loop_new] = CCC_norm_lowess_v2(data_ref,data_all_loop,parameters_LOWESS_loop,flags_loop);           

            
            data_int_loop_FLAG_norm(:,i_T) = data_all_loop_new(:,1);
            data_int_loop_MYC_norm(:,i_T)  = data_all_loop_new(:,2);
            toc
        end
            
        data_channel.FLAG_norm(:,:,i_Rep)  = data_int_loop_FLAG_norm;
        data_channel.MYC_norm(:,:,i_Rep)   = data_int_loop_MYC_norm;
        
    end
    
    clear data_dum_FLAG_all data_dum_MYC_all data_dum_FLAG_affected data_dum_MYC_affected
    
else

   %-- Loop over replicates
    for i_Rep = 1:N_REPEATS_TOR 
      
        fprintf('\nProcessing replicate %d of %d', i_Rep, N_REPEATS_TOR); 
        
        for i_T = 1:par.N_T;
            fprintf('\n  Processing time-point %d of %d \n', i_T, par.N_T);
            tic
                        
            parameters_LOWESS_loop = parameters_LOWESS;                    
            time_str               = num2str(par.time_vect(i_T));      

            %- Select data
            data_dum = [];
            data_dum(:,1) = data_channel.FLAG(ind_Rap1_not_targets,i_T,i_Rep);
            data_dum(:,2) = data_channel.MYC(ind_Rap1_not_targets,i_T,i_Rep);
            data_ref      = data_dum(1:N_proc,:);

            data_all_loop = [];
            data_all_loop(:,1) = data_channel.FLAG(:,i_T,i_Rep);
            data_all_loop(:,2) = data_channel.MYC(:,i_T,i_Rep);

            %- Process
            parameters_LOWESS_loop.file_name_start = ['CCC_Lowess_Rep',num2str(i_Rep), '_T' ,time_str];
            parameters_LOWESS_loop.path_save       = path_save;
                        
            [data_all_loop_new] = CCC_norm_lowess_v2(data_ref,data_all_loop,parameters_LOWESS_loop,flags_loop);           

            
            data_int_loop_FLAG_norm(:,i_T) = data_all_loop_new(:,1);
            data_int_loop_MYC_norm(:,i_T)  = data_all_loop_new(:,2);
            toc
        end
            
        data_channel.FLAG_norm(:,:,i_Rep)  = data_int_loop_FLAG_norm;
        data_channel.MYC_norm(:,:,i_Rep)   = data_int_loop_MYC_norm;
        
    end
end

fprintf(' DONE ! \n');
 

fprintf('* Lowess normalization (Occupancy) .... this now really takes a while .... ')

% ===== OCCUPANCY DATA
if (par.flag_struct.parallel)
    
    %-- Loop over replicates
    for i_Rep = 1:N_REPEATS_OCC 
      
        fprintf('\nProcessing replicate %d of %d \n', i_Rep, N_REPEATS_TOR);        
        fprintf('  - Processing %d occupancy measurements on %d CPUs. \n', N_OCC,matlabpool('size'));
        fprintf('  - Listed times below are for processing of one occupancy measurement by one CPU. \n');
        
        data_dum_INPUT_all = data_channel.INPUT(:,:,i_Rep);
        data_dum_IP_all  = data_channel.IP(:,:,i_Rep);
    
        data_dum_INPUT_affected = data_channel.INPUT(ind_Rap1_not_targets,:,i_Rep);
        data_dum_IP_affected  = data_channel.IP(ind_Rap1_not_targets,:,i_Rep);       
        
        parfor i_OCC = 1:N_OCC;
            tic
            parameters_LOWESS_loop = parameters_LOWESS;          

            %- Select data
            data_dum = [];
            data_dum(:,1) = data_dum_INPUT_affected(:,i_OCC);            
            data_dum(:,2) = data_dum_IP_affected(:,i_OCC);
            
            data_ref      = data_dum(1:N_proc,:);

            data_all_loop = [];
            data_all_loop(:,1) = data_dum_INPUT_all(:,i_OCC);            
            data_all_loop(:,2) = data_dum_IP_all(:,i_OCC);
   
            %- Process
            parameters_LOWESS_loop.file_name_start = ['OCCUPCANYC_Lowess_Rep',num2str(i_Rep), '_OCC' ,num2str(i_OCC)];
            parameters_LOWESS_loop.path_save       = path_save;
            
            [data_all_loop_new] = CCC_norm_lowess_v2(data_ref,data_all_loop,parameters_LOWESS_loop,flags_loop);           

            
            data_int_loop_Input_norm(:,i_OCC) = data_all_loop_new(:,1);
            data_int_loop_IP_norm(:,i_OCC)    = data_all_loop_new(:,2);
            toc
        end
            
        data_channel.INPUT_norm(:,:,i_Rep) = data_int_loop_Input_norm;
        data_channel.IP_norm(:,:,i_Rep)    = data_int_loop_IP_norm;
        
    end

    clear data_dum_INPUT_all data_dum_IP_all data_dum_INPUT_affected data_dum_IP_affected

else 
    
      %-- Loop over replicates
    for i_Rep = 1:N_REPEATS_OCC 
      
        fprintf('\nProcessing replicate %d of %d \n', i_Rep, N_REPEATS_TOR);        

        
        for i_OCC = 1:N_OCC;
            fprintf('\n  Processing occupancy measurement %d of %d \n', i_OCC, N_OCC);
            tic
            parameters_LOWESS_loop = parameters_LOWESS;          

            %- Select data
            data_dum = [];
            data_dum(:,1) = data_channel.INPUT(ind_Rap1_not_targets,i_OCC,i_Rep);
            data_dum(:,2) = data_channel.IP(ind_Rap1_not_targets,i_OCC,i_Rep);
            data_ref      = data_dum(1:N_proc,:);

            data_all_loop = [];
            data_all_loop(:,1) = data_channel.INPUT(:,i_OCC,i_Rep);
            data_all_loop(:,2) = data_channel.IP(:,i_OCC,i_Rep);

            %- Process
            parameters_LOWESS_loop.file_name_start = ['OCCUPCANYC_Lowess_Rep',num2str(i_Rep), '_OCC' ,num2str(i_OCC)];
            parameters_LOWESS_loop.path_save       = path_save;
                        
            [data_all_loop_new] = CCC_norm_lowess_v2(data_ref,data_all_loop,parameters_LOWESS_loop,flags_loop);           

            
            data_int_loop_Input_norm(:,i_OCC) = data_all_loop_new(:,1);
            data_int_loop_IP_norm(:,i_OCC)    = data_all_loop_new(:,2);
            toc
        end
            
        data_channel.INPUT_norm(:,:,i_Rep) = data_int_loop_Input_norm;
        data_channel.IP_norm(:,:,i_Rep)    = data_int_loop_IP_norm;
        
    end
end

clear data_dum data_ref data_all data_all_new
clear data_int_loop_FLAG_norm data_int_loop_MYC_norm data_int_loop_Input_norm data_int_loop_IP_norm

disp('... FINSIHED!!!')


%% Calculate new turn-over ratios and occupancy ratios
data_TOR.norm     = data_channel.MYC_norm ./ data_channel.FLAG_norm;
data_TOR.norm_avg = mean(data_TOR.norm,3);

data_OCC.norm     = data_channel.IP_norm ./ data_channel.INPUT_norm;
data_OCC.norm_avg = mean(data_OCC.norm,3);



%% ========================================================================
%  [7] SAVE TURN-OVER DATA
%  ========================================================================


%% Prepare data-structure for saving turn-over
occ_label      = num2cell(log2(data_OCC.norm_avg(link_probes_genome_IND_in_turn_over,1)));
occ_label_cell = cellfun(@num2str, occ_label,'UniformOutput',false);

%% Assign data
data_struct.data        = log2(data_TOR.norm_avg(link_probes_genome_IND_in_turn_over,:));  % data_TOR mapped back on genome annotation
data_struct.time        = par.time_vect; 
data_struct.probe_label = [link_probes_genome_chr,link_probes_genome_start,link_probes_genome_end,occ_label_cell]; 
data_struct.probe_head  = {'Chr','start','end','Occup'};  


%% Generate output
results_norm.data_TOR     = data_TOR;
results_norm.data_OCC     = data_OCC;
results_norm.data_channel = data_channel;
results_norm.data_struct  = data_struct;



%% Parallel computing - close MATLAB session for parallel computation 
if( par.flag_struct.parallel)
    matlabpool close;
end


fprintf('Lowess normalization FINISHED! \n')
