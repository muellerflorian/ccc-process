function CCC_save_turn_over_v1(file_name_full,data_struct)
% Function to write turn-over data to a file

% === data_struct ... structure with all the data. Containing the following fields
% data        ... matrix with data (each row is one probe)
% time        ... vector with time-points [in min]
% probe_label ... cell with description of probes 
%                   !!! Have to be STRINGS (even for numbers)
%                   !!! at least one element
% probe_head  ... cell with probe header 
%                   !!! at least one element
%=== Get data
data        = data_struct.data;
time        = data_struct.time; 
probe_label = data_struct.probe_label; 
probe_head  = data_struct.probe_head;  


%=== File-name
current_dir = pwd;

%- Ask for file-name if it's not specified
if isempty(file_name_full)
    
    %- Ask user for file-name
    file_name_default = ['CCC_turn_over.txt'];
    [file_save,path_save] = uiputfile(file_name_default,'Save turn-over data');
    file_name_full = fullfile(path_save,file_save);
    
else   
    file_save = 1;
end


%=== Write to file
if file_save ~= 0
        
    N_T = size(data,2);    
    N_P = size(probe_head,2)*size(probe_head,1);        % Either row or column cell
    
    %=== Generate string to write out data
    
    %- Generate header and string to write out data
    string_description = probe_head{1};
    string_write       = '%s';
    
    %- Probe identificiation
    for i_P = 2:N_P
       str_dum1 = ['\t',probe_head{i_P}];
       string_description = [string_description,str_dum1]; 
       
       str_dum2 = '\t%s';
       string_write = [string_write,str_dum2];   
        
    end        
        
    %- Time points identificiation
    for i_T = 1:N_T
       str_dum1 = ['\t',num2str(time(i_T))];
       string_description = [string_description,str_dum1]; 
       
       str_dum2 = '\t%f';
       string_write = [string_write,str_dum2];
    end
    string_description = [string_description,'\n']; 
    string_write       = [string_write,'\n']; 
    
    %=== Generate cell-structures for write-out
    cell_data    = num2cell(data');  %   Each spot is therfore save in a column rather than a row  
            
    %- Combine cell arrays
    cell_write = [probe_label';cell_data];
    
    
    %=== Write to file
    fid = fopen(file_name_full,'w');
    
    fprintf(fid, string_description);    
    fprintf(fid, string_write,cell_write{:});           
    
    fclose(fid);
end
    

cd(current_dir)
    
