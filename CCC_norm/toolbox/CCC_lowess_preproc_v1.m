function results_preproc = CCC_lowess_preproc_v1(file_name,path_name,par)

fprintf('\n===== Pre-processing for LOWESS NORMALIZATION - follow status updates below \n');


%% Parallel computing - open MATLAB session for parallel computation 
if (par.flag_struct.parallel)
    isOpen = matlabpool('size') > 0;
    if (isOpen==0)
        matlabpool open;
    end
end


%% ========================================================================
%  [1] Load and assign SINGLE CHANNEL DATA
%  ========================================================================

%== [1.a] LOAD DATA

fprintf('* Read file with single channel data: %s ... ',file_name.channel)


if file_name.channel ~= 0
    
    %- Open file identifier
    fid             = fopen(fullfile(path_name.channel,file_name.channel));
    
    %- Read-in header 
    tline = fgetl(fid);
    
    %- Analyze header
    header_line    = tline;
    ind_log_tab    = (header_line == sprintf('\t'));
    num_cols       = sum(ind_log_tab);
    
    %- Read in data
    str_read_in = ['%s',repmat('%f', 1, num_cols)];

    %- Read in data
    data_all_struct = textscan(fid, str_read_in,'HeaderLines',0,'delimiter','\t','CollectOutput',1);

    %- Extract relevant data
    data_all        = data_all_struct{2};
    identifier_all  = data_all_struct{1};
    N_id_all        = size(identifier_all,1);
    clear data_all_struct
    
    fclose(fid);
    fprintf(' DONE ! \n');
else
    fprintf(' FAILED !!!!! \n');
    
end


%== [2.a] Extract data for different time-points and occupancy in correct order

%  Matrix links columns in single channel data to different time-points and 
%  the FLAG and MYC labels. Each row in link_col_TOR specifies one 
%  time-point (as specified in time_vect) and contains the column number 
%  of the channels (FLAG and MYC) in text file containing the single channel data.

fprintf('* Extract channel information: ... ')

% === Loop over time-points and assign to data structure
link_col_TOR_dum = par.link_col_TOR - 1;      % Shifted by one due to first column that specifies probe names
N_REPEATS_TOR = size(link_col_TOR_dum,2) / 2; % Number of time-points

for i_T = 1:par.N_T;
     
   %- Loop over repeats
   
   for i_Rep = 1:N_REPEATS_TOR 
        
       %- Indices and data
        ind_Ex_FLAG = link_col_TOR_dum(i_T,i_Rep*2-1);
        ind_Ex_MYC  = link_col_TOR_dum(i_T,i_Rep*2);

        data_channel.FLAG(:,i_T,i_Rep)   =  data_all(:,ind_Ex_FLAG);
        data_channel.MYC(:,i_T,i_Rep)    =  data_all(:,ind_Ex_MYC);
   end
   
end
   
%- Calculate turn-over ratio (data_TOR)
data_TOR.raw     = data_channel.MYC ./ data_channel.FLAG;
data_TOR.raw_avg = mean(data_TOR.raw,3);


%=== OCCUPANCY from conventional ChIP experiment
link_col_OCC_dum = par.link_col_OCC - 1;          % Shifted by one due to first column that specifies probe names
N_REPEATS_OCC    = size(link_col_OCC_dum,2) / 2;  % Number of time-points
N_OCC            = size(par.link_col_OCC,1);      % Number of time-points

%- Loop over occupancy data
for i_occ = 1:N_OCC;
            
    for i_Rep = 1:N_REPEATS_OCC 
        
        ind_INPUT  = link_col_OCC_dum(i_occ,i_Rep*2-1);
        ind_IP     = link_col_OCC_dum(i_occ,i_Rep*2);
        
        data_channel.INPUT(:,i_occ,i_Rep) = data_all(:,ind_INPUT);
        data_channel.IP(:,i_occ,i_Rep)    = data_all(:,ind_IP);
    end
            
end

data_OCC.raw     = data_channel.IP ./ data_channel.INPUT;
data_OCC.raw_avg = mean(data_OCC.raw,3);

%- Clear temporary variables
clear data_all data_Ex1_FLAG data_Ex1_MYC

fprintf(' DONE ! \n');


%% ========================================================================
%  [3] Determine two groups for normalization: affected and not affected probes
%  ========================================================================

%% [3.a] Load list of probes to be affected by protein
% Text file contains a list of all probes that are in the vicinity of a
% known protein binding site. 5th column contains the same probe identifier 
% as used in the single channel intensity data. See above for more details.

    
fprintf('* Read file with affected probes: %s ...',file_name.probes_affected)

if file_name.probes_affected ~= 0

    fid               = fopen(fullfile(path_name.probes_affected, file_name.probes_affected));
    format_string     = '%s %s %f %f %s';
    data_peaks_struct = textscan(fid, format_string,'delimiter', '\t','HeaderLines',0);
    fclose(fid);

    identifier_RAP1 = data_peaks_struct{5};
    N_ind_RAP       = size(identifier_RAP1,1);

    clear data_peaks_struct;
    fprintf(' DONE ! \n');
else
    fprintf(' FAILED !!!!! \n');
end

%% [3.b] Divide probes into two group
%  Loop over all probes in the single channel intensity data and check if
%  they are within the reference group or not. 

fprintf('* Dividing probes in affected and not affected .... might take some time .... ')

ind_Rap1_targets = zeros(size(N_ind_RAP,1));

%- Parallel computing
if( par.flag_struct.parallel )

    parfor ind_target = 1: N_ind_RAP

        index_dum = find(strcmpi(identifier_RAP1{ind_target},identifier_all));

        if not(isempty(index_dum))
            ind_Rap1_targets(ind_target,1) = index_dum;
        end

    end
    
%- Normal computing    
else
    fprintf('\n Processing probes (total,processed): %d / %s', N_ind_RAP, '0');
    str_old = num2str(0);       
    
    for ind_target = 1: N_ind_RAP
        
        if not(rem(ind_target,50))
            str_write = [repmat('\b', 1, length(str_old)), '%s'];
            str_new = num2str(ind_target);
            fprintf(str_write, str_new)
            str_old = str_new;
         end

        index_dum = find(strcmpi(identifier_RAP1{ind_target},identifier_all));

        if not(isempty(index_dum))
            ind_Rap1_targets(ind_target,1) = index_dum;
        end

    end
end

ind_Rap1_all         = (1:N_id_all)';
ind_Rap1_not_targets = setdiff(ind_Rap1_all,ind_Rap1_targets);

fprintf(' DONE ! \n');


%% ========================================================================
%  [4] Load file to LINK PROBE DATA TO GENOME
%  ========================================================================

%% [4.a] Load data to link probes to genome 

fprintf('* Read file linking probe identifier to genome: %s ...',file_name.probes_ident)

if file_name.probes_ident ~= 0

    fopen(fullfile(path_name.probes_ident, file_name.probes_ident));
    format_string = '%s %s %s %s';
    data_lk_pr_genome = textscan(fid, format_string,'delimiter', '\t','HeaderLines',1);
    fclose(fid);

    link_probes_genome_chr    = data_lk_pr_genome{1};
    link_probes_genome_start  = data_lk_pr_genome{2};  % CELL ARRAY OF CHARACTERs NOT INTEGER !!!!!
    link_probes_genome_end    = data_lk_pr_genome{3};
    link_probes_genome_ident  = data_lk_pr_genome{4};

    N_probes_link = length(link_probes_genome_end);

    clear data_lk_pr_genome;
    fprintf(' DONE ! \n');
else
    fprintf(' FAILED !!!!! \n');
end


%% [4.b] Link probes to turn-over data

fprintf('* Link probes to genomic location .... might take some time .... ')

%- Parallel computing
if ( par.flag_struct.parallel)
    parfor ind_probes = 1: N_probes_link

        index_dum = find(strcmpi(link_probes_genome_ident{ind_probes},identifier_all));

        if not(isempty(index_dum))
            link_probes_genome_IND_in_turn_over(ind_probes,1) = index_dum;
        end

    end    
    
%- Normal computing    
else
    
    fprintf('\n Processing probes (total,processed): %d / %s', N_probes_link, '0');
    str_old = num2str(0);  
    
    
    for ind_probes = 1: N_probes_link

        if not(rem(ind_probes,100))
            str_write = [repmat('\b', 1, length(str_old)), '%s'];
            str_new = num2str(ind_probes);
            fprintf(str_write, str_new)
            str_old = str_new;
        end

        index_dum = find(strcmpi(link_probes_genome_ident{ind_probes},identifier_all));

        if not(isempty(index_dum))
            link_probes_genome_IND_in_turn_over(ind_probes,1) = index_dum;
        end

    end
end

fprintf(' DONE ! \n');

%% Assing output results
results_preproc.ind_Rap1_not_targets = ind_Rap1_not_targets;
results_preproc.N_REPEATS_TOR        = N_REPEATS_TOR;
results_preproc.N_REPEATS_OCC        = N_REPEATS_OCC;
results_preproc.data_channel         = data_channel;
results_preproc.data_TOR             = data_TOR;
results_preproc.data_OCC             = data_OCC;
results_preproc.N_OCC                = N_OCC;


results_preproc.link_probes_genome_IND_in_turn_over = link_probes_genome_IND_in_turn_over ;
results_preproc.link_probes_genome_chr   = link_probes_genome_chr;
results_preproc.link_probes_genome_end   = link_probes_genome_end;
results_preproc.link_probes_genome_start = link_probes_genome_start;


%% Parallel computing - close MATLAB session for parallel computation 
if( par.flag_struct.parallel)
    matlabpool close;
end

fprintf('Pre-processing FINISHED!')
