function  [data_all_new data_ref_norm loess_adj_par] = CCC_norm_lowess_v2(data_ref,data_all,parameters,flags)

%- LOWESS normalization for chip data

%  data_ref   ... reference data to calculate parameters
%  data_all   ... Entire data_set that shoudl be normalized


% === Parameters
window_size     = parameters.window_size;
file_name_start = parameters.file_name_start; 
path_save       = parameters.path_save;


%===== REFERENCE DATA

%- Get data from reference data-set       
xi = data_ref(:,1);
xj = data_ref(:,2);        

N_ref = length(xi);

%- Calc lowess on reference data 
M_ref        = log2(xi./xj);
A_ref        = 0.5*( log2(xi.*xj));        
M_ref_smooth = smooth(A_ref,M_ref,window_size/N_ref,'lowess');

%- Identify unique values for A
[A_ref_unique ind_unique] = unique(A_ref); 

%- Correct
M_ref_new = M_ref - M_ref_smooth;

%- Correct data     
xi_norm = 2.^(A_ref+M_ref_new/2);
xj_norm = 2.^(A_ref-M_ref_new/2);

data_ref_norm(:,1) = xi_norm;
data_ref_norm(:,2) = xj_norm;

loess_adj_par(:,1) = A_ref_unique;
loess_adj_par(:,2) = M_ref_smooth(ind_unique);

%===== ENTIRE DATA set
if not(isempty(data_all))
    xi_all = data_all(:,1);
    xj_all = data_all(:,2);

    %- Calculate M-A values and smooth
    M_all = log2(xi_all./xj_all);
    A_all = 0.5*( log2(xi_all.*xj_all));
    M_all_intp                = interp1(A_ref_unique,M_ref_smooth(ind_unique),A_all,'linear','extrap');

    %- Correct
    M_all_new = M_all - M_all_intp;

    xi_all_new = 2.^(A_all+M_all_new/2);
    xj_all_new = 2.^(A_all-M_all_new/2);

    data_all_new(:,1) = xi_all_new;
    data_all_new(:,2) = xj_all_new;
else
    data_all_new = [];
end


%- Plot results if specified
if flags.plot || flags.save
    
    %== Reference data
    
    %- M-A values after normalization
    M_ref_after = log2(xi_norm./xj_norm);
    A_ref_after = 0.5*( log2(xi_norm.*xj_norm));    

    %- Plot results
    h_ref = figure;
    subplot(2,1,1)
    [AA,ind_A] = sort(A_ref);
    hold on
    plot(AA,M_ref(ind_A),'b+')
    plot(AA,M_ref_smooth(ind_A),'r-')
    title('Reference: LOWESS')
    box on
    v = axis;
    xlabel('A = 0.5 * log2(FLAG*MYC)')
    ylabel('M = 0.5 * log2(FLAG/MYC)')
    
    subplot(2,1,2)
    plot(A_ref_after,M_ref_after,'+b')
    title('Reference: after normalization')
    axis(v)
    xlabel('A = 0.5 * log2(FLAG*MYC)')
    ylabel('M = 0.5 * log2(FLAG/MYC)')
    
    set(h_ref,'Position',[560   681   600   600]);
    set(h_ref,'PaperPosition', [0.2    2.5   6    6*600/600])
    set(h_ref,'Color', 'w')
    
    if flags.save
         filename_save = fullfile(path_save,[file_name_start,'_REF']);
         
         if flags.save == 1
            saveas(h_ref,filename_save,'tiff') 
         elseif flags.save == 2
             saveas(h_ref,filename_save,'eps') 
         end
         
         close(h_ref)
    end
   
    
    %== All data
    
    if not(isempty(data_all))
        %- M-A values after normalization
        M_all_after = log2(xi_all_new./xj_all_new);
        A_all_after = 0.5*( log2(xi_all_new.*xj_all_new));

        %- Plot results
        h_all = figure;
        subplot(2,1,1)
        plot(A_all,M_all,'+b')
        title('Entire data-set: before normalization')
        v = axis;
        xlabel('A = 0.5 * log2(FLAG*MYC)')
        ylabel('M = 0.5 * log2(FLAG/MYC)')
        
        subplot(2,1,2)
        plot(A_all_after,M_all_after,'+b')
        title('Entire data-set: after normalization')
        axis(v)
        xlabel('A = 0.5 * log2(FLAG*MYC)')
        ylabel('M = 0.5 * log2(FLAG/MYC)')
        
        set(h_all,'Position',[560   681   600   600]);
        set(h_all,'PaperPosition', [0.2    2.5   6    6*600/600])
        set(h_all,'Color', 'w')

        if flags.save
             filename_save = fullfile(path_save,[file_name_start,'_ALL']);
             
             if flags.save == 1
                saveas(h_all,filename_save,'tiff')  
             elseif flags.save == 2
                saveas(h_all,filename_save,'eps') 
             end
             
             close(h_all)
        end
    end
    
    
end