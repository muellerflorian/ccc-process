%% MATLAB SCRIPT TO normalize competition chip data
%
% Data is normalized with modified Lowess normalization described in
% Lickwar et al, Nature. 2012 Apr 11;484(7393):251-5. 
%
% Processing steps are setup for example data-set that can be
% found on the same website than this code. Change code accordingly for
% your own data. 
%
% Florian Mueller, muef@gmx.net
% v2, August 2012
%
% ==== NOTES and basic workflow
% - Best used in CELL-MODE (see Matlab help file). This allows executing 
%   one block with code after the other. 
%
% - Example data can be used to familiarize yourself with the script
%
% ========= INPUT DATA
%
%  [1] SINGLE CHANNEL INTENSITY DATA 
%      - TAB (!!!) delimited text file 
%      - First row is header row. Each column has to have a header.
%      - Subsequent rows contain single channel data (MYC and FLAG)
%      - First column is probe identifier
%      - File can contain replicates. Will be processed separetely and then 
%        averaged together 
%      - Other rows contain data of single channels for each time-point.
%        No specific ordering of data is necessary. Will be specified 
%        separately separately in matrix 'link_col_T'
%      
%
%      - Example: Rap1_PairFilesCompiled_v1.txt
%
%  [2] LIST OF PROBES AFFECTED BY PROTEIN BINDING
%      This text file specifies the probes that will be excluded from the
%      calculation of the lowess function. We determined these probes by 
%      first determining Rap1 binding sites by Chip-Seq and then considered 
%      all probes within +/- 2000 bp of these sites to be affected by Rap1.
%
%      - TAB delimited text file
%      - No header row      
%      - Each row specifies one probe
%      - 1st column: identifier  of the corresponding site (peak), 2nd col:
%        chromosome location of the probe, 3rd and 4th col: start and end
%        coordinate of probe, 5th col: probe identifier (same one as used in
%        first data-set
%     
%      - Example: Rap1_ChipSeq_Peaks_4000bp_v1.txt
%
%  [3] TEXT FILE LINKING PROBES TO GENOMIC LOCATION
%       Text files  links probe identifier to their genomic location. First
%       column contains the chromosome, second column contains the start
%       position, third column the end position, fourth column the probe
%       identifier.
%
%      - Example: Probes_To_Saccer2_v1.txt
%
%
% == RESULTS
%
%  results_norm is a structure containg several fields with the results of
%  the analysis.
%
%    == results_norm.data_channel : Single channel data
%        Contains single channel data for each 
%        replicate as well as their average.
%
%    == data.TOR : Turnover ratio
%        Contains the raw and normalized turnover ratio for each replicate  
%        as well as their average.
%
%    == results_norm.data_OCC  :   Occupancy data
%        Contains the raw and normalized occupancy data for each replicate  
%        as well as their average.
%
%    == results_norm.data_struct : Summary structure
%        Contains summary that will be saved to text file that can be
%        analyzed with ccc_analyze.
%
%  The turnover data can then be saved as a text file that can be read with
%  CCC_analyze to fit turnover at specific loci. This text file contains
%  for each probe the chromosome, the start and end position, the occupancy
%  as measured by chip-chip, and the turnover time-course
%  - Example: CCC_turn_over_120828.txt

%% Prepare workspace
clear all; close all; clc


%% [1] Define file name of SINGLE CHANNEL INTENSITY DATA
%      Example: Rap1_PairFilesCompiled_v1.txt
[file_name.channel path_name.channel] = uigetfile('*.txt','Define text file with single channel data.');


%% [2] Define file name of list with probes that are affected by binding
%      Example: Rap1_ChipSeq_Peaks_4000bp_v1.txt
[file_name.probes_affected path_name.probes_affected] = uigetfile('*.txt','Define text file text file with affected probes.');


%% [3] Load file linking probes to genomic location
%      - Example: Probes_To_Saccer2_v1.txt
[file_name.probes_ident path_name.probes_ident] = uigetfile('*.txt','Definetext file with probe annotation.');


%% [4] Definition of some parameters 
     
%== Vector with time points in minutes
par.time_vect = [0,10,20,30,40,50,60,90,120,150];
par.N_T       = length(par.time_vect);      % Number of time-points


%== MATRIX LINKS single channel data to replicates, time-points and labels
% + Each row in link_col_T specifies one time-point (as specified in time_vect) 
% + Every column corresponds to one label of one replicate. They area
%   always grouped in two and their sequence is FLAG and then MYC. In this
%   way multiple replicates can be considered. In the example below the
%   first column specifies FLAG of replicate 1, the second column MYC of
%   the first replicate, the third column FLAG of the second replicate, and
%   the fourth column MYC of the second replicate. A third replicate could
%   be accordingly added. So here we specify: FLAG1 - MYC1 - FLAG2 - MYC2 
                 
par.link_col_TOR = [42 43 25 24 ; ...
                    44 34 23 22 ; ...
                    38 39 17 16 ; ...
                    46 47 21 20 ; ...
                    36 37 15 14 ; ...
                    40 41 19 18 ; ...
                    26 27 13 12 ; ...
                    48 49 11 10 ; ...
                    32 33 7  6  ; ...
                    30 31 5  4  ];                               

N_row        = size(par.link_col_TOR,1);      % Number of time-points

if N_row ~= par.N_T
    warndlg('Number of rows in link_col_T has to correspond to number of time points.')
end

%=== MATRIX LINKS single channel data to occupancy measurement. 
%  The occupancy will be reported in the final results as a first value. 
% + Each row corresponds to one repeat of the occupancy measurement.
% + Columns are arranged as specified for turnover measurement to
%   accomodate multiple replicates in groups of two. First the column wiht
%   the input and then the column with the actual IP is measured.
% + If not separate ChIP experiment was performed the competition chip data
%   at t = 0 min can be used as an alternative. Specify competitor (MYC) as input,
%   and endogenous (FLAG as IP).

par.link_col_OCC = [34 35 9 8;
                    28 29 3 2];  
                

%% [5] Flag for parallel computing
%   Script supports parallel computing. This requires (a) the Parallel
%   computing toolbox of Matlab (b) a computer with multiple cores. If the
%   Matlab Parallel toolbox is not installed setting the flag to 1 will
%   result in an error message and the script will not execute. 
par.flag_struct.parallel = 0;     % 0: NO; 1: YES   


%% [6] Pre-processing of data
%  This involves loading the data, probe definition, mapping the probes in
%  the genome, and identifying probes that are affected by binding. 
results_preproc = CCC_lowess_preproc_v1(file_name,path_name,par);


%% [7] Apply lowess normalization
%  Lowess normalization can crash with parallel-computing. Best is to try
%  it works, if not disable.
par.flag_struct.parallel = 0; 

%- Window size for lowess normalization
par.LOWESS.window_size  = 1000;

%  === MA plots to inspect quality of normalization
%  Flags to specify if summary plots of lowess normalizaton should be saved
%  These files show the MA plot of each data-set before and after 
%  normalization for the reference group and the the entire data-set.
%  Images are saved as .tif files in a subdirectory of the directory where
%  the single channel data is saved. The directory is called _Plots_120824
%  where the last six digits indicates the date as YYMMDD. File names are
%  generated automatically and look like CCC_Lowess_Rep1_T0_ALL.tif. Where
%  Rep1 indicates Replicate 1, T0 time 0 min, ALL refers to the entire data
%  -set (while REF is only the reference data used to calculate the LOWESS
%  function). 
% + Example: see plots in folder _Plots_120828

par.flag_struct.plot = 1;
par.flag_struct.save = 1;   % 0 Don't save image, 1 save image as tif file, 2 save image as eps (saved 

%- Perform normalization
[results_norm] = CCC_lowess_apply_v1(results_preproc,path_name,par);


%% [8] Save results
%  This generates a text file that can be read by ccc_analyze to fit the 
%  turnover data. It is a tab deliminted text file. First column contains
%  the chromosome of the probe, second column and third column the start
%  and end position of the probe, the forth column the averaged occupancy
%  at this probe, the subsequent columns contain the turnover data. 
%  - Example: CCC_turn_over_120828.txt
CCC_save_turn_over_v1([],results_norm.data_struct)
